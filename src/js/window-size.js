/**
 * Detect window size
 */
var windowWidth = $(window).width();

// TODO: this is causing a console error
window.addEventListener('resize', function () {
    windowWidth = $(window).width();
    // console.log(windowWidth);
    detectOrientation();
});