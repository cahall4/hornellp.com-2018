var map;

function CreateMap(properties, locationCount) {
	var mapOptions;
	if (locationCount == 1) {
		var latlong = properties[0].split('|');
		mapOptions = {
			center: new google.maps.LatLng(latlong[4], latlong[5]),
			zoom: 14,
			disableDefaultUI: true,
			zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	}
	else {
		mapOptions = {
			center: new google.maps.LatLng(32.29876, -90.18481),
			zoom: 7,
			disableDefaultUI: true,
			zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	}
	map = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
	var bounds = new google.maps.LatLngBounds();
	var infoWindow = new google.maps.InfoWindow();
	for (var i = 0; i < locationCount; ++i) {
		var info = properties[i].split('|');
		var marker = new google.maps.Marker({ position: new google.maps.LatLng(info[4], info[5]), map: map });
		var text = $("#locationPopupDiv").html();
		text = text.replace("#LOCATION_NAME#", info[0]);
		text = text.replace("#STREET_ADDRESS#", info[1]);
		text = text.replace("#CITY_STATE_ZIP#", info[2]);
		text = text.replace("#LOCATION_URL#", info[3]);
		makeInfoWindowEvent(map, infoWindow, text, marker);
		bounds.extend(new google.maps.LatLng(info[4], info[5]));
	}

	if (locationCount != 1) {
		map.fitBounds(bounds);
	}

	var listener = google.maps.event.addListener(map, "idle", function () {
		if (map.getZoom() > 15) map.setZoom(15);
		google.maps.event.removeListener(listener);
	});
}

function makeInfoWindowEvent(map, infowindow, contentString, marker) {
	google.maps.event.addListener(marker, 'click', function () {
		infowindow.setContent(contentString);
		infowindow.open(map, marker);
	});
}