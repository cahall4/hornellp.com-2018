/**
 * People dropdowns
 * 
 * @requires jQuery slim
 * @requires /src/js/functions.js
 */
var $dropdownFocusAreas = $('#dropdownFocusAreas')

if ($dropdownFocusAreas.length) {
    $dropdownFocusAreas.find('.btn').on('click', function (e) {
        showSpecialDropdown('menuFocusAreas', e)
    })
    $('#menuFocusAreas > .btn, html').on('click', function () {
        hideSpecialDropdown()
    })
    $('#dropdownLastName').find('.btn').on('click', function (e) {
        showSpecialDropdown('menuLastName', e)
    })
    $('#menuLastName > .btn, html').on('click', function () {
        hideSpecialDropdown()
    })

    $('#menuLastName').find('.btn').on('click', function () {
        detectOrientation()
        teamMenuRespond(windowWidth)
    })

    teamMenuRespond(windowWidth)
    $(window).resize(function () {
        detectOrientation()
        teamMenuRespond(windowWidth)
    })
}

/**
 * Show/hide people list blocks
 * 
 * @requires jQuery slim (sorry, we're in a hurry)
 * @requires /src/js/functions.js
 */
var $people = $('.people-list-person');

if ($people.length) {
    $people.find('.people-list-link').on('click', function(e) {
        e.preventDefault();
        var $hexTeamListWrapper = $('#hexTeamListWrapper');
        var height = $hexTeamListWrapper.height();
        $hexTeamListWrapper.css('height', height);
        $people.removeClass('open');
        $(this).closest('.people-list-person').addClass('open').parent('.row-people-list').addClass('has-open');
    });
    $people.find('.close-reveal-wrapper').on('click', function(e) {
        e.stopPropagation();
        var $hexTeamListWrapper = $('#hexTeamListWrapper');
        $hexTeamListWrapper.css('height', 'auto');
        $people.removeClass('open').parent('.row-people-list').removeClass('has-open');
    });
}