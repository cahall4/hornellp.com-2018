/**
 * Media queries in javascript!
 * Mimics Bootstrap 4 media queries:
 * https://getbootstrap.com/docs/4.1/layout/overview/#responsive-breakpoints
 * 
 * Example use:
 * 
 * if (mq_up_sm.matches) {
 *   // true: window width is at least 576px
 * } else {
 *   // false: window width is less than 576px
 * }
 * 
 */

// min-width
const mq_up_sm = window.matchMedia( "(min-width: 576px)" );
const mq_up_md = window.matchMedia( "(min-width: 768px)" );
const mq_up_lg = window.matchMedia( "(min-width: 992px)" );
const mq_up_xl = window.matchMedia( "(min-width: 1200px)" );

// max-width
const mq_down_xs = window.matchMedia( "(max-width: 575.98px)" );
const mq_down_sm = window.matchMedia( "(max-width: 767.98px)" );
const mq_down_md = window.matchMedia( "(max-width: 991.98px)" );
const mq_down_lg = window.matchMedia( "(max-width: 1199.98px)" );

// only
const mq_only_xs = window.matchMedia( "(max-width: 575.98px)" );
const mq_only_sm = window.matchMedia( "(min-width: 576px) and (max-width: 767.98px)" );
const mq_only_md = window.matchMedia( "(min-width: 768px) and (max-width: 991.98px)" );
const mq_only_lg = window.matchMedia( "(min-width: 992px) and (max-width: 1199.98px)" );
const mq_only_xl = window.matchMedia( "(min-width: 1200px)" );