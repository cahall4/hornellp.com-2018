/**
 * Navbar search
 */

var siteHeader = document.getElementById('siteHeaderV2');
var navbarSearch = document.getElementById('navbarSearch');
var navbarSearchInput = document.getElementById('navbarSearchInput');
var navbarBtnSearchClose = document.getElementById('btnSearchClose');
var navMain = document.getElementById('navbarMainContent');
var contactBtn = document.getElementById('navbarContactBtn');
var isNavSearchBlurred = false;

// Open the search...
if (navbarSearchInput) {
    
    navbarSearchInput.addEventListener('focus', function() {
        this.setAttribute('placeholder', 'What are you looking for?');
        addClass(siteHeader, 'overflow-x-hidden');
        addClass(navMain, 'hide-me');
        addClass(navbarSearch, 'slide-over');
        addClass(contactBtn, 'slide-over');
        removeClass(navbarBtnSearchClose, 'hide-me');
    });

    // Close the search...
    navbarSearchInput.addEventListener('blur', function() {
        this.setAttribute('placeholder', 'Search');
        removeClass(navMain, 'hide-me');
        removeClass(contactBtn, 'slide-over');
        removeClass(navbarSearch, 'slide-over');
        addClass(navbarBtnSearchClose, 'hide-me');
        isNavSearchBlurred = true;
    });
    
}

if (contactBtn && navbarSearchInput) {
    contactBtn.addEventListener('transitionend', function() {
        if (isNavSearchBlurred) {
            removeClass(siteHeader, 'overflow-x-hidden');
            isNavSearchBlurred = false;
        }
    });
}


/**
 * Footer search
 */

var footerSearchInput = document.getElementById('footerSearchInput');
var footerBtnSearchClose = document.getElementById('footerBtnSearchClose');
var isFooterSearchBlurred = false;

// Open the search...
if (footerSearchInput) {

    footerSearchInput.addEventListener('focus', function() {
        isFooterSearchBlurred = true;
    });

    // Close the search...
    footerSearchInput.addEventListener('blur', function() {
        addClass(footerBtnSearchClose, 'hide-me');
        isFooterSearchBlurred = false;
    });

    footerSearchInput.addEventListener('transitionend', function() {
        if (isFooterSearchBlurred) {
            removeClass(footerBtnSearchClose, 'hide-me');
            isFooterSearchBlurred = false;
        }
    });

}