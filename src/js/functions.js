/**
 * Class functions from
 * https://plainjs.com/javascript/attributes/adding-removing-and-testing-for-classes-9/
 * 
 * @param {string} el 
 * @param {string} className 
 */
function hasClass(el, className) {
    return el.classList ? el.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(el.className);
}
function addClass(el, className) {
    if (el.classList) el.classList.add(className);
    else if (!hasClass(el, className)) el.className += ' ' + className;
}
function removeClass(el, className) {
    if (el.classList) el.classList.remove(className);
    else el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
}

/**
 * Detect screen orientation
 */
function detectOrientation () {
    window.deviceOrientation = '' // global
    var height = $(window).height();
    var width = $(window).width();
    if (width > height) {
        // Landscape
        window.deviceOrientation = 'landscape';
    } else {
        // Portrait
        window.deviceOrientation = 'portrait';
    }
    console.log(window.deviceOrientation);
}
  
/**
 * Show/hide special dropdowns
 */
function showSpecialDropdown (thisMenu, e) {
    $('.dropdown-special').css('visibility', 'hidden');
    document.getElementById(thisMenu).style.visibility = 'visible';
    e.preventDefault();
    e.stopPropagation();
}
function hideSpecialDropdown () {
    $('.dropdown-special').css('visibility', 'hidden');
}
  
/**
 * Team dropdown responsive behavior
 */
function teamMenuRespond (winWidth) {
    // console.log('teamMenuRespond called');
    var $menuLastName = $('#menuLastName');
    // $menuLastName.find('li > ul').removeClass('open');
    if (winWidth < 769) {
        // console.log('Handling touch.');
        $menuLastName.find('ul > li').on('click', function (e) {
            $menuLastName.find('li > ul').removeClass('open');
            $(this).find('ul').addClass('open');
            e.preventDefault();
            e.stopPropagation();
            $(this).find('a').on('click', function () {
                var thisUrl = $(this).attr('href');
                if ($(this).hasClass('fancybox')) {
                $.fancybox.open({
                    href: thisUrl,
                    type: 'ajax',
                    maxWidth: 800,
                    maxHeight: 600,
                    fitToView: true,
                    width: '70%',
                    height: '70%'
                })
                } else {
                    window.location = thisUrl;
                }
            })
        })
    } else {
        // console.log('Not handling touch.');
        $menuLastName.find('ul > li').off().find('ul').removeClass('open');
    }
}