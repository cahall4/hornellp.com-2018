# HORNE 2018 Website Templates

These are the 2018 template files for HORNE LLP. There are really two big sections:

1. The plain ol' template files in the `src` folder made up of Pug, SCSS, and JS (they compile to `dist`)
2. The actual Umbraco template files that live in the `Umbraco` folder

## Working on the templates...

For the plain HTML templates we're using:

- Pug (https://pugjs.org/)
- Bootstrap 4.0.0 (need to udpate, https://getbootstrap.com/)
- Gulp (https://gulpjs.com/)
- NPM (https://www.npmjs.com/)

You should be able to clone this repository, run `npm install`, and then `gulp`, and away you go!

__NOTE:__ Since there's only one section actually built out the moment, http://localhost:8080/healthcare/ is all that's currently working.

## Working in Umbraco...

Umbraco is a .NET CMS that uses C# razor for templating:

- Umbraco (https://umbraco.com/)
- Razor (https://our.umbraco.com/documentation/getting-started/design/templates/basic-razor-syntax)
- Razor enhancement for VS Code (https://marketplace.visualstudio.com/items?itemName=austincummings.razor-plus)

__WARNING!__ Some of the Umbraco templates use the old styling from the previous site, and they ARE NOT INCLUDED in this repository. We're in the process of transitioning the old site layout to the new site layout.

## Client review

I'm currently loading up my work to http://horne2018.addisonhall.com/v5/healthcare/ on my server so that everyone at HORNE can review. I can provide FTP access so that you can sync up your work as well.

## Using Git (need help!)

TODO: Figure out how to sync up and down?

Have you done this before? Once we're working in the same repository, do we just do a pull before we start working to sync up?